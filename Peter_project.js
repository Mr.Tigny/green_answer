let question1 = document.getElementById('question1');
let question2 = document.getElementById('question2');
let question3 = document.getElementById('question3');
let question4 = document.getElementById('question4');
let question5 = document.getElementById('question5');
let question6 = document.getElementById('question6');
let question7 = document.getElementById('question7');
let question8 = document.getElementById('question8');
let question9 = document.getElementById('question9');
let question10 = document.getElementById('question10');
let validation = document.getElementById('validation');
let fleche1 = document.getElementById('question1__fleche1');
let fleche2 = document.getElementById('question2_phone__fleche2');
let fleche3 = document.getElementById('question3_phone__fleche3');
let fleche4 = document.getElementById('question4_phone__fleche4');
let fleche5 = document.getElementById('question5_phone__fleche5');
let fleche6 = document.getElementById('question6_phone__fleche6');
let fleche7 = document.getElementById('question7_phone__fleche7');
let fleche8 = document.getElementById('question8_phone__fleche8');
let fleche9 = document.getElementById('question9_phone__fleche9');
let fleche10 = document.getElementById('question10_phone__fleche10');
let question2_phone = document.getElementById('question2_phone');
let question3_phone = document.getElementById('question3_phone');
let question4_phone = document.getElementById('question4_phone');
let question5_phone = document.getElementById('question5_phone');
let question6_phone = document.getElementById('question6_phone');
let question7_phone = document.getElementById('question7_phone');
let question8_phone = document.getElementById('question8_phone');
let question9_phone = document.getElementById('question9_phone');
let question10_phone = document.getElementById('question10_phone');
let fin = document.getElementById('fin');
let resultat = document.getElementById('resultat');
let check1 = document.getElementById('question1__check1');
let check1_2 = document.getElementById('question1__check1_2');
let check1_3 = document.getElementById('question1__check1_3');
let check2 = document.getElementById('question2__check2');
let check2_2 = document.getElementById('question2__check2_2');
let check2_3 = document.getElementById('question2__check2_3');
let check3 = document.getElementById('question3__check3');
let check3_2 = document.getElementById('question3__check3_2');
let check3_3 = document.getElementById('question3__check3_3');
let check4 = document.getElementById('question4__check4');
let check4_2 = document.getElementById('question4__check4_2');
let check4_3 = document.getElementById('question4__check4_3');
let check5 = document.getElementById('question5__check5');
let check5_2 = document.getElementById('question5__check5_2');
let check5_3 = document.getElementById('question5__check5_3');
let check6 = document.getElementById('question6__check6');
let check6_2 = document.getElementById('question6__check6_2');
let check6_3 = document.getElementById('question6__check6_3');
let check7 = document.getElementById('question7__check7');
let check7_2 = document.getElementById('question7__check7_2');
let check7_3 = document.getElementById('question7__check7_3');
let check8 = document.getElementById('question8__check8');
let check8_2 = document.getElementById('question8__check8_2');
let check8_3 = document.getElementById('question8__check8_3');
let check9 = document.getElementById('question9__check9');
let check9_2 = document.getElementById('question9__check9_2');
let check9_3 = document.getElementById('question9__check9_3');
let check10 = document.getElementById('question10__check10');
let check10_2 = document.getElementById('question10__check10_2');
let check10_3 = document.getElementById('question10__check10_3');

//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche1.onclick = function() {
    question1.classList.add('hidden');
    question2_phone.style.display = 'block';
    question2_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche2.onclick = function() {
    
    question2_phone.style.display = 'none';
    question3_phone.style.display = 'block';
    question3_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche3.onclick = function() {
    question3_phone.style.display = 'none';
    question4_phone.style.display = 'block';
    question4_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche4.onclick = function() {
    question4_phone.style.display = 'none';
    question5_phone.style.display = 'block';
    question5_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche5.onclick = function() {
    question5_phone.style.display = 'none';
    question6_phone.style.display = 'block';
    question6_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche6.onclick = function() {
    question6_phone.style.display = 'none';
    question7_phone.style.display = 'block';
    question7_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche7.onclick = function() {
    question7_phone.style.display = 'none';
    question8_phone.style.display = 'block';
    question8_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche8.onclick = function() {
    question8_phone.style.display = 'none';
    question9_phone.style.display = 'block';
    question9_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre celle d'apres en la faisant rebondir
fleche9.onclick = function() {
    question9_phone.style.display = 'none';
    question10_phone.style.display = 'block';
    question10_phone.classList.add('bounce');
}
//clique sur fleche,cache la div de la question et fait apparaitre "Resultat" et le carré blanc de fin
fleche10.onclick = function() {
    question10_phone.style.display = 'none';
    fin.style.display = 'block';
    resultat.style.display = 'block';
}


window.onload = function() {
    question1.classList.add('bounce');
}

check1.onclick = function() {
    question2.classList.add('bounce');
    question1.classList.remove('bounce');
}

check1_2.onclick = function() {
    question2.classList.add('bounce');
    question1.classList.remove('bounce');
}

check1_3.onclick = function() {
    question2.classList.add('bounce');
    question1.classList.remove('bounce');
}

question1.onmouseover = function() {
    question1.classList.remove('bounce');
}

check2.onclick = function() {
    question3.classList.add('bounce');
    question2.classList.remove('bounce');
}

check2_2.onclick = function() {
    question3.classList.add('bounce');
    question2.classList.remove('bounce');
}

check2_3.onclick = function() {
    question3.classList.add('bounce');
    question2.classList.remove('bounce');
}

question2.onmouseover = function() {
    question2.classList.remove('bounce');
}

check3.onclick = function() {
    question4.classList.add('bounce');
    question3.classList.remove('bounce');
    window.scrollTo(50, 950);
}

check3_2.onclick = function() {
    question4.classList.add('bounce');
    question3.classList.remove('bounce');
    window.scrollTo(50, 950);

}

check3_3.onclick = function() {
    question4.classList.add('bounce');
    question3.classList.remove('bounce');
    window.scrollTo(50, 950);
}

question3.onmouseover = function() {
    question3.classList.remove('bounce');
}

question3.onmouseout = function() {
    question3.classList.remove('bounce');
}

check4.onclick = function() {
    question5.classList.add('bounce');
    question4.classList.remove('bounce');
}

check4_2.onclick = function() {
    question5.classList.add('bounce');
    question4.classList.remove('bounce');
}

check4_3.onclick = function() {
    question5.classList.add('bounce');
    question4.classList.remove('bounce');
}

question4.onmouseover = function() {
    question4.classList.remove('bounce');
}

check5.onclick = function() {
    question6.classList.add('bounce');
    question5.classList.remove('bounce');
}

check5_2.onclick = function() {
    question6.classList.add('bounce');
    question5.classList.remove('bounce');
}

check5_3.onclick = function() {
    question6.classList.add('bounce');
    question5.classList.remove('bounce');

}

question5.onmouseover = function() {
    question5.classList.remove('bounce');
}

check6.onclick = function() {
    question7.classList.add('bounce');
    question6.classList.remove('bounce');
    window.scrollTo(50, 1800);
}

check6_2.onclick = function() {
    question7.classList.add('bounce');
    question6.classList.remove('bounce');
    window.scrollTo(50, 1800);
}

check6_3.onclick = function() {
    question7.classList.add('bounce');
    question6.classList.remove('bounce');
    window.scrollTo(50, 1800);
}

question6.onmouseover = function() {
    question6.classList.remove('bounce');
}

check7.onclick = function() {
    question8.classList.add('bounce');
    question7.classList.remove('bounce');
}

check7_2.onclick = function() {
    question8.classList.add('bounce');
    question7.classList.remove('bounce');
}

check7_3.onclick = function() {
    question8.classList.add('bounce');
    question7.classList.remove('bounce');
}


question7.onmouseover = function() {
    question7.classList.remove('bounce');
}


check8.onclick = function() {
    question9.classList.add('bounce');
    question8.classList.remove('bounce');
}

check8_2.onclick = function() {
    question9.classList.add('bounce');
    question8.classList.remove('bounce');
}

check8_3.onclick = function() {
    question9.classList.add('bounce');
    question8.classList.remove('bounce');
}


question8.onmouseover = function() {
    question8.classList.remove('bounce');
}

check9.onclick = function() {
    window.scrollTo(50, 2200);
    question10.classList.add('bounce');
    question9.classList.remove('bounce');
}

check9_2.onclick = function() {
    window.scrollTo(50, 2200);
    question10.classList.add('bounce');
    question9.classList.remove('bounce');
}

check9_3.onclick = function() {
    window.scrollTo(50, 2200);
    question10.classList.add('bounce');
    question9.classList.remove('bounce');
}


question9.onmouseover = function() {
    question9.classList.remove('bounce');
}

check10.onclick = function() {
    question10.classList.remove('bounce');
    validation.classList.add('bounce');
    window.scrollTo(50, 2400);
}

check10_2.onclick = function() {
    question10.classList.remove('bounce');
    validation.classList.add('bounce');
    window.scrollTo(50, 2400);
}

check10_3.onclick = function() {
    question10.classList.remove('bounce');
    validation.classList.add('bounce');
    window.scrollTo(50, 2400);
}

question10.onmouseover = function() {
    question10.classList.remove('bounce');
}

validation.onmouseover = function() {
    validation.classList.remove('bounce');
}
validation.onmouseout = function() {
    validation.classList.add('bounce');
}




